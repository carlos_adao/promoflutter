import 'package:font_awesome_flutter/font_awesome_flutter.dart';

List categories = [
  {
    "name": "Higiene pessoal",
    "icon": "assets/icon_hig.png",
    "items": 5
  },
  {
    "name": "Limpeza",
    "icon": "assets/icon_limp.png",
    "items": 20
  },
  {
    "name": "Bebidas",
    "icon": "assets/icon_beb.png",
    "items": 9
  },
  {
    "name": "Carnes",
    "icon": "assets/icon_carn.png",
    "items": 5
  },
  {
    "name": "Sucos e frutas",
    "icon": "assets/icon_suc_frut.png",
    "items": 15
  },
  {
    "name": "Hortifruti",
    "icon": "assets/icon_hort.png",
    "items": 15
  },
  {
    "name": "Padaria",
    "icon": "assets/icon_padaria.png",
    "items": 15
  },
  {
    "name": "Utensílios",
    "icon": "assets/icon_uten.png",
    "items": 15
  },
  {
    "name": "Latícineos",
    "icon": "assets/icon_lat.png",
    "items": 15
  },
  {
    "name": "Pets",
    "icon": "assets/icon_pet.png",
    "items": 15
  },
  {
    "name": "Promoções",
    "icon": "assets/shopping-bag.png",
    "items": 15
  },
  {
    "name": "Todas  ",
    "icon": "assets/icon_all.png",
    "items": 15
  }
];