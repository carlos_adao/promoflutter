import 'package:flutter/material.dart';
import 'package:promo/screens/categories_screen.dart';
import 'package:promo/ui/categoria.dart';
import 'package:promo/ui/home_category.dart';
import 'package:promo/ui/paytm.dart';
import 'package:promo/ui/splashscreen.dart';

import 'constant/Constant.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      theme: new ThemeData(primaryColor: Color(0xff243A61),
      ),
      routes: <String, WidgetBuilder>{
        SPLASH_SCREEN: (BuildContext context) => SplashScreen(),
        PAY_TM: (BuildContext context) => Paytm()
      },
    );
  }
}
