import 'package:flutter/material.dart';

class GridModel {
  int     _idEmp;
  String _imagePath;
  String _banerPath;
  String _title;
  Color _color;

  GridModel(this._idEmp, this._imagePath, this._banerPath, this._title,
      this._color);

  Color get color => _color;

  set color(Color value) {
    _color = value;
  }

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  String get imagePath => _imagePath;

  set imagePath(String value) {
    _imagePath = value;
  }

  int get idEmp => _idEmp;

  set idEmp(int value) {
    _idEmp = value;
  }

  String get banerPath => _banerPath;

  set banerPath(String value) {
    _banerPath = value;
  }


}