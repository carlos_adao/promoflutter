import 'package:flutter/material.dart';
import 'package:promo/screens/details.dart';
import 'package:promo/ui/smooth_star_rating.dart';
import 'package:promo/util/const.dart';



class CartItem extends StatelessWidget {
  final String name;
  final String img;
  final String emp_logo;
  final bool isFav;
  final double rating;
  final int raters;


  CartItem({
    Key key,
    @required this.name,
    @required this.img,
    @required this.emp_logo,
    @required this.isFav,
    @required this.rating,
    @required this.raters})
      :super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
      child: InkWell(
        onTap: (){
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context){
                return ProductDetails();
              },
            ),
          );
        },
        child: Container(
          child: Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 0.0, right: 10.0),
                child: Container(
                  height: MediaQuery.of(context).size.width/3.5,
                  width: MediaQuery.of(context).size.width/3,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.asset(
                      "$img",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Container(

                width: MediaQuery.of(context).size.width/2.5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[

                    Text(
                      "$name",
                      style: TextStyle(
//                    fontSize: 15,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Row(
                      children: <Widget>[
                        SmoothStarRating(
                          starCount: 1,
                          color: Constants.ratingBG,
                          allowHalfRating: true,
                          rating: 5.0,
                          size: 12.0,
                        ),
                        SizedBox(width: 6.0),
                        Text(
                          "5.0 (23 Reviews)",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10.0),
                    Row(
                      children: <Widget>[
                        Text(
                          "20 Pieces",
                          style: TextStyle(
                            fontSize: 11.0,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        SizedBox(width: 10.0),

                        Text(
                          r"$90",
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.w900,
                            color: Theme.of(context).accentColor,
                          ),
                        ),

                      ],
                    ),

                    SizedBox(height: 10.0),

                    Text(
                      "Quantity: 1",
                      style: TextStyle(
                        fontSize: 11.0,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],

                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image.asset(
                      "$emp_logo",
                      width: 50,
                      height: 30,
                      color: Colors.black54,
                    ),
                  ],

                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
