import 'package:flutter/material.dart';
import 'package:promo/model/Gridmodel.dart';
import 'package:promo/util/categories.dart';
import 'package:promo/util/foods.dart';
import 'grid_product.dart';
import 'home_category.dart';

var id;

class Categoria extends StatefulWidget {
  GridModel emp;

  Categoria(this.emp);

  @override
  State<StatefulWidget> createState() {
    return new CategoriaState();
  }

}

class CategoriaState extends State<Categoria> {

  static final TextStyle _boldStyle =
      new TextStyle(fontWeight: FontWeight.bold);
  static final TextStyle _greyStyle = new TextStyle(color: Colors.grey);
  String catie = "Promoções";

  final ddlValues = <int>[1, 2, 3, 4];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: new CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          new SliverAppBar(
            expandedHeight: 180.0,
            pinned: true,
            flexibleSpace: new FlexibleSpaceBar(
              title: Text(widget.emp.title),
              background:
                  new Image.asset(widget.emp.banerPath, fit: BoxFit.cover),
            ),
          ),
          new SliverPadding(
              padding: const EdgeInsets.symmetric(vertical: 2.0),
              sliver: new SliverPadding(
                  padding: const EdgeInsets.symmetric(vertical: 2.0),
                  sliver: SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          Container(
                              height: 800.0,
                              child: ListView(
                                children: <Widget>[
                                  SizedBox(height: 10.0),
                                  Container(
                                    height: 65.0,
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      shrinkWrap: true,
                                      itemCount: categories == null
                                          ? 0
                                          : categories.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        Map cat = categories[index];
                                        return HomeCategory(
                                          icon: cat['icon'],
                                          title: cat['name'],
                                          items: cat['items'].toString(),
                                          isHome: false,
                                          tap: () {
                                            setState(() {
                                              catie = "${cat['name']}";
                                            });
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                  SizedBox(height: 20.0),
                                  Text(
                                    "$catie",
                                    style: TextStyle(
                                      fontSize: 23,
                                      fontWeight: FontWeight.w800,
                                    ),
                                  ),
                                  Divider(),
                                  SizedBox(height: 10.0),
                                  GridView.builder(
                                    shrinkWrap: true,
                                    primary: false,
                                    physics: NeverScrollableScrollPhysics(),
                                    gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      childAspectRatio:
                                      MediaQuery.of(context).size.width /
                                          (MediaQuery.of(context).size.height /
                                              1.25),
                                    ),
                                    itemCount: foods == null ? 0 : foods.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      Map food = foods[index];
                                      return GridProduct(
                                        img: food['img'],
                                        isFav: false,
                                        name: food['name'],
                                        rating: 5.0,
                                        raters: 23,
                                      );
                                    },
                                  ),
                                ],
                              )),

                          // Scrollable horizontal widget here
                        ],
                      )))),
        ],
      ),
    );
  }
  //
}
