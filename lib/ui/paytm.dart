import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:promo/model/Gridmodel.dart';
import 'package:promo/model/ImageSliderModel.dart';
import 'package:promo/ui/categoria.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'main_screen.dart';

class Paytm extends StatefulWidget {
  @override
  _PaytmState createState() => _PaytmState();
}

class _PaytmState extends State<Paytm> {
  int _currentIndex = 0;
  int _currentIndexUp = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: _appBar(),
        body: _bodyItem(),
        backgroundColor: Colors.grey[200],
        bottomNavigationBar: _bottemTab());
  }

  Widget _appBar() {
    return new AppBar(
      backgroundColor: Color(0xff243A61),
      elevation: 0.0,
      title: Text("Promo"),
      centerTitle: true,
      leading: IconButton(
        icon: Icon(Icons.menu),
        onPressed: () {},
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.search,
            size: 26.0,
          ),
          onPressed: () {
            /*Navigator.of(context).push(
                // With MaterialPageRoute, you can pass data between pages,
                // but if you have a more complex app, you will quickly get lost.
                MaterialPageRoute(builder: (context) => Categoria(),)
            );*/
          },
        )
      ],
    );
  }

  Widget _bottemTab() {
    return new BottomNavigationBar(
        currentIndex: 0,
        type: BottomNavigationBarType.fixed,
        items: [
          new BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text(
                'perfil',
              )),
          new BottomNavigationBarItem(
              icon: Image.asset(
                "assets/shopping-bagg.png",
                width: 24.0,
                height: 24.0,
              ),
              title: Text(
                'Lista',
              )),
          new BottomNavigationBarItem(
              icon: Image.asset(
                "assets/qr-code.png",
                width: 24.0,
                height: 24.0,
              ),
              title: Text(
                'Scan',
              )),
          new BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              title: Text(
                'Favoritos',
              )),
          new BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              title: Text(
                'Config',
              )),
        ]);
  }

  /*WID GET COM AS EMPRESAS NO SLID TOOL BAR*/
  Widget _bodyItem() {
    return SingleChildScrollView(
      child: new Column(
        children: <Widget>[
          Container(
              width: double.maxFinite,
              color: Color(0xff243A61),
              child: Container(
                child: CarouselSlider(
                  reverse: false,
                  aspectRatio: 5,
                  viewportFraction: 1.0,
                  initialPage: 0,
                  enlargeCenterPage: true,
                  autoPlay: false,
                  onPageChanged: (index) {
                    setState(() {
                      _currentIndexUp = index;
                      print(_currentIndexUp);
                    });
                  },
                  items: List<GridView>.generate((2), (int index) {
                    return GridView.count(
                      crossAxisCount: 4,
                      children: List<GridItemTop>.generate((4), (int index) {
                        return GridItemTop(_getGridList()[index + (_currentIndexUp * 4)]);
                      }),
                    );
                  }),
                ),
              )

//            GridView.builder(
//              gridDelegate:
//                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),
//              scrollDirection: Axis.horizontal,
//              itemCount: _getGridList().length,
//              itemBuilder: (context, index) {
//                return GridList(_getGridList()[index]);
//              },
//            ),
              ),
          Container(
            color: Color(0xff243A61),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(2, (int index) {
                return dots(_currentIndexUp, index);
              }),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 1),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              height: 40,
              width: MediaQuery.of(context).size.width,
            ),
          ),
          GridView.count(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            crossAxisCount: 4,
            children: List<GridItem>.generate(12, (int index) {
              return GridItem(_getGridItemList()[index]);
            }),
          ),
          /*Carrocel que mostra as ofertas que estão em promoção do dia*/
          Padding(
            padding: const EdgeInsets.only(top: 0, bottom: 5),
            child: Container(
              color: Colors.white,
              child: CarouselSlider(
                aspectRatio: 2,
                viewportFraction: 1.0,
                initialPage: 0,
                autoPlayInterval: Duration(seconds: 2),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                pauseAutoPlayOnTouch: Duration(seconds: 2),
                enlargeCenterPage: true,
                autoPlay: true,
                onPageChanged: (index) {
                  setState(() {
                    _currentIndex = index;
                  });
                },
                items: CarouselSliderList(_getImageSliderList()),
              ),
            ),
          ),
        ],
      ),
    );
  }



  List<GridModel> _getGridItemList() {
    List<GridModel> list = new List<GridModel>();
    list.add(new GridModel(1, "assets/icon_hig.png", "", "Higiene\npessoal", null));
    list.add(new GridModel(2, "assets/icon_limp.png", "", "Limpesa", null));
    list.add(new GridModel(3, "assets/icon_beb.png", "", "Bebidas", null));
    list.add(new GridModel(4, "assets/icon_carn.png", "", "Carnes", null));
    list.add(new GridModel(5, "assets/icon_suc_frut.png", "", "Sucos e frutas", null));
    list.add(new GridModel(6, "assets/icon_hort.png", "", "Hortifrut", null));
    list.add(new GridModel(7, "assets/icon_padaria.png", "", "Padaria", null));
    list.add(new GridModel(8, "assets/icon_uten.png", "", "Utensílios", null));
    list.add(new GridModel(9, "assets/icon_lat.png", "", "Latícineos", null));
    list.add(new GridModel(10, "assets/icon_pet.png", "", "Pets", null));
    list.add(new GridModel(11, "assets/shopping-bag.png", "", "Promoções", null));
    list.add(new GridModel(12, "assets/icon_all.png", "", "Todas", null));
    return list;
  }

  //LISTA COM AS EMPRESAS QUE SÃO EXIBIDAS NO TOPO
  List<GridModel> _getGridList() {
    List<GridModel> list = new List<GridModel>();
    list.add(new GridModel(0, "assets/logo_itao.png","assets/bn_itao.png", "Itão", Colors.white));
    list.add(new GridModel(1, "assets/logo_bem_melhor.png","assets/bn_bm.png", "Bem Melhor", Colors.white));
    list.add(new GridModel(2, "assets/logo_meira.png", "assets/bn_meira.png", "Meira", Colors.white));
    list.add(new GridModel(3, "assets/logo_rondelli.png", "assets/bn_rondelli.png", "Rondelli", Colors.white));
    list.add(new GridModel(4, "assets/logo_alana.png", "assets/bn_alana.png", "Alana", Colors.white));
    list.add(new GridModel(5, "", "", "", Colors.white));
    list.add(new GridModel(6, "", "", "", Colors.white));
    list.add(new GridModel(7, "", "", "", Colors.white));
    return list;
  }

  List<ImageSliderModel> _getImageSliderList() {
    List<ImageSliderModel> list = new List();
    list.add(new ImageSliderModel("assets/promord01.png"));
    list.add(new ImageSliderModel("assets/promobm01.png"));
    list.add(new ImageSliderModel("assets/promoit01.png"));
    list.add(new ImageSliderModel("assets/promoal01.png"));
    list.add(new ImageSliderModel("assets/promome01.png"));
    return list;
  }


  CarouselSliderList(List<ImageSliderModel> getImageSliderList) {
    return getImageSliderList.map((i) {
      return Builder(builder: (BuildContext context) {
        return imageSliderItem(i);
      });
    }).toList();
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  Widget dots(int current, index) {
    if (current != index) {
      return Container(
          width: 8.0,
          height: 8.0,
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: dotColor(index),
          ));
    } else {
      return Container(
          width: 8.0,
          height: 8.0,
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(10),
              color: dotColor(index)));
    }
  }

  /*CONJUNTOS DE IMAGENS QUE SÃO CARREGADOS NO CARROCEL*/
  Widget imageSliderItem(ImageSliderModel i) {
    return Container(
        padding: EdgeInsets.only(left: 8, right: 8),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child:incluiStack()
          //persoImage(i.path)
          //Image.asset(
          //  i.path,
          //  fit: BoxFit.cover,
          //),
        ));
  }

  Widget incluiStack() {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Center(
          child: CachedNetworkImage(
            imageUrl: 'https://i.imgur.com/xVS07vQ.jpg',
            placeholder: (context, url) =>
            const CircularProgressIndicator(),
            errorWidget: (context, url, error) => const Icon(Icons.error),
            fadeOutDuration: const Duration(seconds: 1),
            fadeInDuration: const Duration(seconds: 3),
            height: 300,  
            width: 500,
          ),

           /*ProgressiveImage(
            placeholder: AssetImage('assets/logo_itao.png'),
            // size: 1.87KB
            thumbnail: NetworkImage('https://i.imgur.com/7XL923M.jpg'),
            // size: 1.29MB
            image: NetworkImage('https://i.imgur.com/xVS07vQ.jpg',
            ),
            height: 300,
            width: 500,
          ),*/
        ),

        Positioned(
          bottom: 30.0,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0),
            ),
            child: Text('Clique para prosseguir'),
            onPressed: () {},
          ),
        )
      ],
    );
  }



  Color dotColor(int index) {
    return _currentIndexUp == index ? Colors.white : Colors.grey;
  }
}

/*GRID COM AS CATEGORIAS CADASTRADAS*/
class GridItem extends StatelessWidget {
  GridModel gridModel;

  GridItem(this.gridModel);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(1 / 2),
      child: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  _categoriaRoute(context);
                },
                child: Container(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child:  Image.asset(
                      gridModel.imagePath,
                      width: 30,
                      height: 30,
                      color: gridModel.color,
                    )
                  ),
                ),
              )
             ,
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  gridModel.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 12),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /*Chama o widget para exibir todos os produtos de uma determinada categoria, de todas as empresas
  * Create by Adão 11/04/2020  josé.carlos.adao@hotmail.com*/
  Route _categoriaRoute(BuildContext context) {
    Navigator.push(context, PageTransition(type: PageTransitionType.leftToRight, child: MainScreen()));
  }
}

/*TOPO COM AS EMPRESAS CADASTRADAS*/
class GridItemTop extends StatelessWidget {
  GridModel gridModel;

  GridItemTop(this.gridModel);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(1 / 2),
      child: Material(
        color: Color(0xff243A61),
        child: InkWell(
          onTap: () {
            _createRoute(context, gridModel);
          },
          child: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    gridModel.imagePath,
                    width: 50,
                    height: 30,
                    color: gridModel.color,
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: Text(
                      gridModel.title,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  /*Chama o widget para exibir uma lista com os produtos de uma determinada empresa
  * Create by Adão 11/04/2020  josé.carlos.adao@hotmail.com*/
  Route _createRoute(BuildContext context, GridModel emp) {
    Navigator.push(context, PageTransition(type: PageTransitionType.leftToRight, child: Categoria(emp)));
  }
}
