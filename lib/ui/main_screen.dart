import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:promo/screens/checkout.dart';
import 'package:promo/ui/search.dart';
import 'badge.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()=>Future.value(false),
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: Icon(
              Icons.keyboard_backspace,
            ),
            onPressed: ()=>Navigator.pop(context),
          ),
          centerTitle: true,
          title: Text("Promo"),
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
              icon: IconBadge(
                icon: Icons.notifications,
                size: 22.0,
              ),
              onPressed: (){
                /*Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context){
                      return Notifications();
                    },
                  )
                );*/
              },
              tooltip: "Notifications",
            ),
          ],
        ),
        body: SearchScreen(),
        floatingActionButton: FloatingActionButton(
          tooltip: "Checkout",
          onPressed: (){
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context){
                  return Checkout();
                },
              ),
            );
          },
          child: Image.asset(
            "assets/shopping-bagg.png",
            width: 24.0,
            height: 24.0,
            color: Colors.white,
          ),
          heroTag: Object(),
        ),
      ),
    );
  }
}
